# PubSub
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Dependencies
- **Ubuntu 20**
- **ROS Galactic** 

## Building and Running the package
**Note-** Be sure ros is sourced before
1. Create your workspace
    ```bash
    mkdir -p <your_ws_name>/src
    ```
2. Clone the repo
    ```bash
    cd <your_ws>/src
    git clone https://gitlab.com/pratik_bhujbal/task1.git
    ```
3. Install ROS dependencies:
    ```bash
    cd ~/<your_ws>
    rosdep install --from-paths src --ignore-src -r -y
    ```

4. Building the package:
    ```bash
    cd ~/<your_ws>
    colcon build
    
    ```
5. Publish sensor data:  
    ```bash
    cd ~/<your_ws>
    source install/setup.bash
    ros2 launch sensors sensors_pub.launch.py
    ```
6. Visualize average data of all sensors (Im new terminal):  
    ```bash
    cd ~/<your_ws>
    source install/setup.bash
    ros2 launch data_processor sensor_dataprocess.launch.py
    ```

## Result
<p align="center">
<img src="info.png" />
</p>
