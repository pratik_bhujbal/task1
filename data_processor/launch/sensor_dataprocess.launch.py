from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    sub_node = Node(
        package="data_processor",
        executable="sensors_sub",
    )
    ld.add_action(sub_node)
    return ld
