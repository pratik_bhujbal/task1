import rclpy
from numpy import mean
from rclpy.node import Node
from std_msgs.msg import Float64
from sensor_msgs.msg import Temperature


class MinimalSubscriber(Node):
    """Class to sunscribe sensors data and log average.

    Args:
        Node : ROS Node
    """

    def __init__(self):
        super().__init__('sensorsdata_sub')
        self.create_subscription(
            Float64,
            'laser_data',
            self.laser_callback,
            25)
        self.create_subscription(
            Temperature,
            'temp_data',
            self.temp_callback,
            25)
        self.create_subscription(
            Float64,
            'speed_data',
            self.speed_callback,
            25)

    def average(self, data_list):
        """To calculate average of sensor data.

        Args:
            data_list (float list): sensor data list

        Returns:
            float: average
        """
        avg = mean(data_list)
        return round(avg, 2)

    def laser_callback(self, msg):
        """Laser data callback.

        Args:
            msg (float64): laser sensor data
        """
        data_list = []
        for i in range(20):
            data_list.append(msg.data)
        avg = self.average(data_list)
        self.get_logger().info('Average laser data: "%s"' % avg)

    def speed_callback(self, msg):
        """Speed data callback.

        Args:
            msg (float64): speed sensor data
        """
        data_list = []
        for i in range(20):
            data_list.append(msg.data)
        avg = self.average(data_list)
        self.get_logger().info('Average speed data: "%s"' % avg)

    def temp_callback(self, msg):
        """Temperature data callback.

        Args:
            msg (float64): temperature sensor data
        """
        data_list = []
        for i in range(20):
            data_list.append(msg.temperature)
        avg = self.average(data_list)
        self.get_logger().info('Average temperature data: "%s"' % avg)


def main():
    """Main function."""
    rclpy.init(args=None)
    minimal_subscriber = MinimalSubscriber()
    rclpy.spin(minimal_subscriber)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
