import os
from glob import glob
from setuptools import setup

package_name = 'data_processor'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='pratik',
    maintainer_email='pbhujbal@umd.edu',
    description='Sensor data subscriber',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': ['sensors_sub=data_processor.sensors_sub:main',
                            ],
    },
)
