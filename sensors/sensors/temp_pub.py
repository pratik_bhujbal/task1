#!/usr/bin/env python3
import random
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Temperature


class TempNode(Node):
    """Class to publish temeprature sensor data.

    Args:
        Node: ROS Node
    """

    def __init__(self):
        super().__init__("temperature_pub")
        self.temperature_publisher_ = self.create_publisher(
            Temperature, "temp_data", 25)
        frequency = 30
        delay = 1 / frequency
        self.create_timer(delay, self.publish_temperature)

    def publish_temperature(self):
        """Publish sensor data."""
        temperature = round(random.uniform(60, 90), 2)
        msg = Temperature()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = "temp_link"
        msg.temperature = temperature
        self.temperature_publisher_.publish(msg)


def main():
    """Main function."""
    rclpy.init(args=None)
    node = TempNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
