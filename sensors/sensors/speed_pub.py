#!/usr/bin/env python3
import rclpy
import random
from rclpy.node import Node
from std_msgs.msg import Float64


class SpeedNode(Node):
    """Class to publish speed sensor data.

    Args:
        Node: ROS Node
    """

    def __init__(self):
        super().__init__("speed_pub")
        self.speed_publisher_ = self.create_publisher(
            Float64, "speed_data", 25)
        frequency = 80  # in hz
        delay = 1 / frequency
        self.create_timer(delay, self.publish_speed)

    def publish_speed(self):
        """Publish sensor data."""
        speed = round(random.uniform(0, 0.75), 2)
        msg = Float64()
        msg.data = speed
        self.speed_publisher_.publish(msg)


def main():
    """Main function."""
    rclpy.init(args=None)
    node = SpeedNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
