#!/usr/bin/env python3
import rclpy
import random
from rclpy.node import Node
from std_msgs.msg import Float64


class LaserNode(Node):
    """Class to publish laser data.

    Args:
        Node: ROS Node
    """

    def __init__(self):
        super().__init__("laser_pub")
        self.laser_publisher_ = self.create_publisher(
            Float64, "laser_data", 25)
        frequency = 35
        delay = 1 / frequency

        self.create_timer(delay, self.publish_laser)

    def publish_laser(self):
        """Publish sensor data."""
        laser = round(random.uniform(0, 10), 2)
        msg = Float64()
        msg.data = laser
        self.laser_publisher_.publish(msg)


def main():
    """Main function."""
    rclpy.init(args=None)
    node = LaserNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
