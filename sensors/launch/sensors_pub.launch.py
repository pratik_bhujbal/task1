from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    l_laser_node = Node(
        package="sensors",
        executable="laser_pub",
    )
    l_temp_node = Node(
        package="sensors",
        executable="temp_pub",
    )
    l_speed_node = Node(
        package="sensors",
        executable="speed_pub",
    )

    ld.add_action(l_laser_node)
    ld.add_action(l_temp_node)
    ld.add_action(l_speed_node)
    return ld
